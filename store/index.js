export const state = () => ({
    token: "",
    color: '',
    text: '',
    snackbar: false
})

export const mutations = {
    addToken(state, token) {
        state.token = token;
    },
    delToken(state) {
        state.token = "";
    },
    closeSnackbar(state, value) {
        state.snackbar = value;
    },
    callSnackbar(state, snackbar) {
        state.text = snackbar.text;
        state.color = snackbar.color;
        state.snackbar = true;
    }
}

// export const actions = {
//     nuxtServerInit({ commit }, { req }) {
//         if (req.headers.cookie) {
            // let token = req.headers.cookie;
            // let str = token.match(/=\s*(.*)/);
//             commit("addToken", str[1]);
//         }
//     }
// }
